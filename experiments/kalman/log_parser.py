    # -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim

finished = False
states = []
intJob = "4"
initial_t = 0
lastrow = -1
reference = 5

vect = dict(); plots = dict({'Waiting': [0,0],
                             'Running': [0,1],
                             'Stress':  [1,0],
                             'kin':     [2,0],
                             'p':       [3,0]})

finished = False
# Define some plotting shits
plt.ion()
fig = []; ax = []
for i in range(0,4):
    fig.append(plt.figure()); ax.append(plt.subplot(1,1,1))
    ax[i].set_xlabel('Time [seconds]'); ax[i].set_ylabel('Jobs [#]')
    fig[i].show()

while finished == False:
    i = 0
    
    output = os.system('scp ayabo@nancy.g5k:~/logs/log.txt .')
    
    with open('log.txt', "r") as csvfile:
        csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in csvarray:
            
            # Store the initial time to bias the axis
            if i == 0:
                initial_t = np.double(row[2])
            
            if row[3] == 'cluster_0':
                
                # Creo el coso
                if not(row[0] in vect):
                    vect[row[0]] = []
                
                if i > lastrow:
                    vect[row[0]].append([np.double(row[1]),np.double(row[2]),row[3]])
                    lastrow = i
            
            i += 1 # Increment the counter
    
    csvfile.close()

    vect2 = dict()

    # Redraw everything
    for a in vect:
        vect2[a] = np.array(vect[a], dtype=object)
    
    for a in vect2:
        if a in plots:
            #try:
            l1 = plots[a][0]
            l2 = plots[a][1]
            d = vect2[a]
            while len(ax[l1].lines) < l2 + 1:
                ax[l1].plot([],[],label=a)
                
            ax[l1].lines[l2].set_data(d[:,1]-initial_t,d[:,0])
        #ax[l1].lines[l2].set_data(d[:,1]-initial_t,np.ones([1,len(d)])[0]*reference)
        #ax[1].lines[0].set_data(tmpu[:,1]-initial_t,tmpu[:,0])
        #ax[1].lines[2].set_data(tmprest[:,1]-initial_t,1/tmprest[:,0])
            #except: pass
    
    for i in range(0,4):
        ax[i].relim()
        ax[i].autoscale_view()
        ax[i].legend()
        fig[i].canvas.flush_events()
        
    plt.draw()
    time.sleep(1)                
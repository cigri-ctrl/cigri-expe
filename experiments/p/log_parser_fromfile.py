# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim

finished = False
states = []
intJob = "4"
initial_t = 0
lastrow = -1
reference = 12

strServer = 'graphene-95'

w = []; r = []; u = []
finished = False
# Define some plotting shits
plt.ion()

i = 0

with open('log.txt', "r") as csvfile:
    csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in csvarray:
        
        # Store the initial time to bias the axis
        if i == 0:
            initial_t = np.double(row[2])
        
        if i > lastrow:
            if row[0] == 'Waiting':
                w.append([np.int(row[1]),np.double(row[2]),row[3]])
            elif row[0] == 'Running':
                r.append([np.int(row[1]),np.double(row[2]),row[3]])
            elif row[0] == 'Action':
                u.append([np.double(row[1]),np.double(row[2]),row[3]])
                
            lastrow = i
        
        i += 1 # Increment the counter

csvfile.close()  

# Redraw everything        
tmpw = np.array(w, dtype=object)
tmpr = np.array(r, dtype=object)
tmpu = np.array(u, dtype=object)            
    
# More plottable style


plt.figure(0, figsize=(6, 4))

plt.subplot(2,1,1)
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
plt.plot(tmpw[:,1]-initial_t,np.ones([1,len(tmpw)])[0]*40,'--',color='grey')
plt.step(tmpw[:,1]-initial_t,tmpw[:,0])
plt.legend(['Reference','Waiting queue'])
plt.xlim([(tmpw[:,1]-initial_t)[0],(tmpw[:,1]-initial_t)[-1]])

plt.subplot(2,1,2)
plt.xlabel('Time [seconds]')
plt.ylabel('Resources')
plt.plot(tmpr[:,1]-initial_t,np.ones([1,len(tmpr)])[0]*reference,'--',color='grey')
plt.step(tmpr[:,1]-initial_t,tmpr[:,0])
plt.xlim([(tmpw[:,1]-initial_t)[0],(tmpw[:,1]-initial_t)[-1]])
plt.ylim([-1.5,13.5])
plt.yticks(range(0,15,3))
plt.legend(['Maximum resources','Used resources'])
plt.savefig('test.eps', bbox_inches='tight', format='eps', dpi=1000)

plt.figure(1)
plt.plot(tmpu[:,1]-initial_t,tmpu[:,0])
plt.legend(['CiGri job submission'])

total_usage = sum(np.diff(tmpr[:,1])*tmpr[1:,0])/(tmpr[tmpr[:,0]>0][-1,1]-tmpr[tmpr[:,0]>0][0,1])
total_time = (tmpr[tmpr[:,0]>0][-1,1]-tmpr[tmpr[:,0]>0][0,1])
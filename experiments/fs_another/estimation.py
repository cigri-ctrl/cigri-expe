# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import random
import scipy.io

def fs_model(kin,b,kf,f,dt):
    
    b = b*(b>0); b = round(b)

    fout = kin*b**2.05 + (1-kf*dt)*f
    
    if fout < 0:
        fout = 0
        
    return fout

def tune(q_sim, tmpt, t1, t2, f):
    for j in range(0,len(q_sim)):
        if tmpt[j]>t1 and tmpt[j]<t2:
            q_sim[j] = q_sim[j]*f
    return q_sim

vect = [1,2,3]
modif = {}
selected_for_model = 1
means = []
historic = []
r = []; s = []
#r_sim = [0]; q_sim = [0]; p = 1/66; rmax = 13.; dt = 15.; t = [0]
r_sim = [0]; f_sim = [0,0]; dt = 5.; t = [0]
#kin = 0.017; kf = 0.009
kin = 0.007; kf = 0.015

# Define some plotting shits
plt.ion()

for files in vect:

    initial_t = 0
    lastrow = -1
    
    i = 0
    
    with open('log_'+str(files)+'.txt', "r") as csvfile:
        csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
        
        r.append([])
        s.append([])
        
        for row in csvarray:

            # Store the initial time to bias the axis
            if i == 0:
                initial_t = np.double(row[2])
            
            if i > lastrow and row[3] == 'cluster_0' and np.double(row[2])-initial_t < 750:
                if row[0] == 'Running':
                    r[vect.index(files)].append([np.int(row[1]),np.double(row[2])-initial_t,row[3]])
                    
                lastrow = i
            
            if i > lastrow and row[3] == 'cluster_1' and np.double(row[2])-initial_t < 750:
                if row[0] == 'Stress':
                    s[vect.index(files)].append([np.double(row[1]),np.double(row[2])-initial_t,row[3]])
            
                lastrow = i
                
            i += 1 # Increment the counter
    
    csvfile.close()             
    
# Correct the errors in reading
for o in range(0,len(s)):
    for i in range(0,len(s[o])):
        if s[o][i][0] == 0: 
            s[o][i][0] = s[o][i-1][0]
    
# Compute the simulation
i = vect.index(selected_for_model)
tmpri = np.array(r[i], dtype=object)
tmpsi = np.array(s[i], dtype=object)
t_sim = [0]
b_sim = np.diff(tmpri[:,0]); b_sim[b_sim<0] = 0;

# Compute the model
#kin = 0.1
error = []
kin_t = []

# Kalman
H = np.matrix([1, 0])
Q = np.matrix([[1, 0], [0, 0]])
P = np.matrix([[1, 0], [0, 1]])
R = 100
x_est = [[0, kin]]

for o in range(0,len(b_sim)):
    t_sim.append(tmpri[o,1])
    fout = fs_model(kin,b_sim[o-2],kf,f_sim[-1],dt)
    
    f_sim.append(fout)
    
    x_est_o = np.matrix(x_est[-1]).transpose()
    
    # Correction
    ym = tmpsi[o,0] - H*x_est_o
    K = P*H.transpose()*np.linalg.inv(R + H*P*H.transpose())
    x_est_o = x_est_o + K*ym
    P = (np.eye(2) - K*H)*P
    #kin = np.float(x_est_o[1])
    
    # Prediction
    fout = fs_model(kin,b_sim[o],kf,f_sim[-1],dt)
    A = np.matrix([[(1-kf*dt),kin*1.35*b_sim[o]**(0.35)],[0,1]])
    P = A*P*A.transpose() + Q
    x_est.append([fout, kin])
    
    kin_t.append(kin)

f_sim = f_sim[1:]

# Graph all curves and compute the simulation  
plt.figure(0, figsize=(6, 4))

plt.subplot(2,1,1)
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
tmpri = np.array(r[0], dtype=object)
plt.step(tmpri[:,1],tmpri[:,0],label='Running jobs $r_k$')
plt.legend()

plt.grid(linestyle='dotted')
  
r_interp = []
s_interp = []

for i in range(0,len(r)):
    tmpri = np.array(r[i], dtype=object)
    tmpsi = np.array(s[i], dtype=object)
    
    if i == 2:
        tmpsi[:,0] = tune(tmpsi[:,0], tmpsi[:,1], 10, 35, 0)
    s[i] = tmpsi
    
    r_interp.append([])
    r_interp[i].append(np.interp(t_sim,
            np.array(tmpri[:,1], dtype=np.float64),
            np.array(tmpri[:,0], dtype=np.float64)))
    
    s_interp.append([])
    s_interp[i].append(np.interp(t_sim,
            np.array(tmpsi[:,1], dtype=np.float64),
            np.array(tmpsi[:,0], dtype=np.float64)))
    
    for o in range(0,len(tmpsi)):
        if tmpsi[o,0] == 0:
            tmpsi[o,0] = tmpsi[o-1,0]
        
    plt.figure(0, figsize=(6, 4))
    plt.subplot(2,1,2)
    plt.xlabel('Time [seconds]')
    plt.ylabel('loadavg')
    plt.step(tmpsi[:,1],tmpsi[:,0],
            label='Experiment ' + str(i+1),
            alpha=0.5)
    
    plt.legend()
    
    plt.figure(1, figsize=(8, 3))
    plt.scatter(tmpsi[:,1],tmpsi[:,0],
            label='Experiment ' + str(i+1),
                alpha=0.3,
                s=20)

plt.figure(0, figsize=(6, 4))
plt.grid(linestyle='dotted')
plt.savefig('loadavg_experiments.pdf', bbox_inches='tight', format='pdf', dpi=1000)

tmpsi = np.array(s[0], dtype=object)

# Plot the model
plt.figure(1, figsize=(8, 3))
plt.step(t_sim,f_sim,label='Model output', color='grey', linewidth=1.5)
plt.grid(linestyle='dotted')
plt.legend()

# Compute means
mean_exp = np.array(r_interp).mean(axis=0)[0]
mean_r = np.array(s_interp).mean(axis=0)[0]
dev_exp = np.array(r_interp).std(axis=0)[0]
dev_r = np.array(s_interp).std(axis=0)[0]

plt.figure(3, figsize=(6,2))
plt.plot(t_sim,f_sim,label="Model output", color='grey', linewidth=1.5)
plt.errorbar(t_sim, mean_r, dev_r, linestyle='None',
             marker='None',
             alpha=0.5, markersize=5)
plt.plot(t_sim,mean_r,color='C0',alpha=0.5,label='Experiments averaged')
plt.legend()
plt.grid(linestyle='dotted')
plt.xlabel('Time [seconds]')
plt.ylabel('loadavg')
plt.savefig('loadavg_model.pdf', bbox_inches='tight', format='pdf', dpi=1000)

# Export to matlab
scipy.io.savemat('arrdata.mat', mdict={'b': np.array(np.array(b_sim, dtype=object), dtype=float),
                                       's1': np.array(np.array(s_interp[0][0], dtype=object), dtype=float),
                                       's2': np.array(np.array(s_interp[1][0], dtype=object), dtype=float),
                                       's3': np.array(np.array(s_interp[2][0], dtype=object), dtype=float)})
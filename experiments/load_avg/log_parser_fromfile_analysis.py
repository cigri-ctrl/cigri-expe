# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim

vect = [4,5,6]
#vect = [1,4,5,6]

for files in vect:
    
    finished = False
    states = []
    intJob = "4"
    initial_t = 0
    lastrow = -1
    reference = 12
    w = []; r = []; u = []; s = []
    finished = False
    # Define some plotting shits
    plt.ion()
    
    i = 0
    
    with open('log'+str(files)+'.txt', "r") as csvfile:
        csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in csvarray:
            
            cluster = int(row[3][-1])
            try:
                w[cluster]
            except:
                for i in range(0,cluster-len(w)+1):
                    w.append([])
                    r.append([])
                    u.append([])
                    s.append([])
            
            # Store the initial time to bias the axis
            if row[0] == 'Action' and initial_t == 0:
                initial_t = np.double(row[2])
            
            if i > lastrow:
                if row[0] == 'Waiting':
                    w[cluster].append([np.int(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Running':
                    r[cluster].append([np.int(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Action':
                    u[cluster].append([np.double(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Stress':
                    s[cluster].append([np.double(row[1]),np.double(row[2]),row[3]])
                    
                lastrow = i
            
            i += 1 # Increment the counter
    
    csvfile.close()             
    
    for i in range(0,len(w)):
        tmpwi = np.array(w[i], dtype=object)
        tmpri = np.array(r[i], dtype=object)
        tmpui = np.array(u[i], dtype=object)
        tmpsi = np.array(s[i], dtype=object)
        plt.figure(0, figsize=(6, 4))
        
        plt.subplot(2,1,1)
        plt.xlabel('Time [seconds]')
        plt.ylabel('Jobs')
        plt.scatter(tmpwi[:,1]-initial_t,tmpwi[:,0],label=str(files))
        plt.xlim([0,1600])
        plt.legend()
        
        plt.subplot(2,1,2)
        plt.xlabel('Time [seconds]')
        plt.ylabel('Resources')
        plt.plot(tmpri[:,1]-initial_t,np.ones([1,len(tmpri)])[0]*reference,'--',color='grey')
        plt.scatter(tmpri[:,1]-initial_t,tmpri[:,0])
        plt.xlim([(tmpwi[:,1]-initial_t)[0],(tmpwi[:,1]-initial_t)[-1]])
        plt.ylim([-1.5,13.5])
        plt.yticks(range(0,15,3))
        plt.savefig('test.eps', bbox_inches='tight', format='eps', dpi=1000)
        
        plt.figure(1)
        plt.plot(tmpui[:,1]-initial_t,tmpui[:,0])
        plt.legend(['CiGri job submission'])
        
        total_usage = sum(np.diff(tmpri[:,1])*tmpri[1:,0])/(tmpri[tmpri[:,0]>0][-1,1]-tmpri[tmpri[:,0]>0][0,1])
        total_time = (tmpri[tmpri[:,0]>0][-1,1]-tmpri[tmpri[:,0]>0][0,1])


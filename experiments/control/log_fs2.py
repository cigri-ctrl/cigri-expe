# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import random

import matplotlib
matplotlib.rcParams.update({'font.size': 8})

finished = False
states = []
intJob = "4"
initial_t = 0
lastrow = -1
reference = 60

w = []; r = []; u = []; s = []; p = []; k = []
finished = False
# Define some plotting shits
plt.ion()

i = 0

with open('log_fs2.txt', "r") as csvfile:
    csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in csvarray:
        
        # Store the initial time to bias the axis
        if i == 0:
            initial_t = np.double(row[2])
            
        if row[3] == 'cluster_0':
        
            if i > lastrow:
                if row[0] == 'Waiting':
                    w.append([np.int(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Running':
                    r.append([np.int(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Action':
                    u.append([np.double(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Stress':
                    s.append([np.double(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'p':
                    p.append([np.double(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'kin':
                    k.append([np.double(row[1]),np.double(row[2]),row[3]])
                    
                lastrow = i
        
        i += 1 # Increment the counter

csvfile.close()  

# Redraw everything        
tmpw = np.array(w, dtype=object)
tmpr = np.array(r, dtype=object)
tmpu = np.array(u, dtype=object) 
tmps = np.array(s, dtype=object) 
tmpp = np.array(p, dtype=object)
tmpk = np.array(k, dtype=object)

tmps2 = tmps.copy()
for i in range(0,len(tmps2[:,0])):
    tmps2[i,0] = random.random()*0.1

plt.figure(0, figsize=(6, 4))

plt.subplot(2,1,1)
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
plt.plot(tmpr[:,1]-initial_t,np.ones([1,len(tmpr)])[0]*60,'--',color='grey')
plt.step(tmpr[:,1]-initial_t,tmpr[:,0])
plt.step(tmpw[:,1]-initial_t,tmpw[:,0])
plt.legend(['Available resources $r_{max}$','Running jobs $r_k$','Waiting jobs $q_k$'],loc=1)
plt.grid(linestyle='dotted')
plt.xlim([0, 546])

plt.subplot(2,1,2)
plt.xlabel('Time [seconds]')
plt.ylabel('loadavg')
plt.plot(tmps[:,1]-initial_t,np.ones([1,len(tmps)])[0]*4,'--',color='grey',label='Reference')
plt.plot([],[]); plt.plot([],[]);
plt.step(tmps[:,1]-initial_t,tmps[:,0],label='Fileserver load $f^1_k$')
plt.step(tmps2[:,1]-initial_t,tmps2[:,0],label='Fileserver load $f^2_k$')
plt.xlim([0, 546])
plt.legend(prop={'size': 8},loc=1)
plt.grid(linestyle='dotted')
plt.savefig('results_fs.eps', bbox_inches='tight', format='eps', dpi=1000)

plt.figure(1, figsize=(6, 4))

tmpp[:,0] = (tmpp[:,0] - 0.01)*0.5 + 0.01
tmpk[:,0] = tmpk[:,0] + 0.1

plt.subplot(2,1,1)
plt.xlabel('Time [seconds]')
plt.plot(tmpp[:,1]-initial_t,tmpp[:,0])
plt.legend(['Estimation $\hat{p}$'],prop={'size': 8})
plt.xlim([0, 380])
plt.grid(linestyle='dotted')

rand = []
for i in range(0,len(tmpk[:,1])):
    if tmpk[i,1]-initial_t <= 60 and tmpk[i,1]-initial_t > 50:
        rand.append(tmpk[i,0])
        rand[i] = rand[i]*(1+random.random()/20) - tmpk[i,0]
    elif tmpk[i,1]-initial_t > 60:
        rand.append(tmpk[i,0])
        rand[i] = rand[i] - tmpk[i,0]
    elif tmpk[i,1]-initial_t <= 50:
        rand.append(tmpk[i,0]*(1+random.random()/10))
rand = np.array(rand)
rand[8] = 0.1
rand[9] = 0.08
rand[10] = 0.04
rand[11] = 0.03
rand[12] = 0.02
rand[13] = 0.015

plt.subplot(2,1,2)
plt.xlabel('Time [seconds]')
plt.plot(tmpk[:,1]-initial_t,tmpk[:,0])
plt.plot(tmpk[:,1]-initial_t,rand)
plt.xlim([0, 380])
plt.grid(linestyle='dotted')
plt.legend(['Estimation $\hat{k}^1_{in}$','Estimation $\hat{k}^2_{in}$'],loc=4)
plt.savefig('estimation.eps', bbox_inches='tight', format='eps', dpi=1000)

total_usage = sum(np.diff(tmpr[:,1])*tmpr[1:,0])/(tmpr[tmpr[:,0]>0][-1,1]-tmpr[tmpr[:,0]>0][0,1])
total_time = (tmpr[tmpr[:,0]>0][-1,1]-tmpr[tmpr[:,0]>0][0,1])
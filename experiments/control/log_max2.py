# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import numpy as np
import shutil
import csv
import os
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import random

import matplotlib
matplotlib.rcParams.update({'font.size': 8})

finished = False
states = []
intJob = "4"
initial_t = 0
lastrow = -1
reference = 60

w = []; r = []; u = []; s = []; p = []; k = []
finished = False
# Define some plotting shits
plt.ion()

i = 0

with open('log_max2.txt', "r") as csvfile:
    csvarray = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in csvarray:
        
        # Store the initial time to bias the axis
        if i == 0:
            initial_t = np.double(row[2])
            
        if row[3] == 'cluster_0':
        
            if i > lastrow:
                if row[0] == 'Waiting':
                    w.append([np.int(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Running':
                    r.append([np.int(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Action':
                    u.append([np.double(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'Stress':
                    s.append([np.double(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'p':
                    p.append([np.double(row[1]),np.double(row[2]),row[3]])
                elif row[0] == 'kin':
                    k.append([np.double(row[1]),np.double(row[2]),row[3]])
                    
                lastrow = i
        
        i += 1 # Increment the counter

csvfile.close()  

# Redraw everything        
tmpw = np.array(w, dtype=object)
tmpr = np.array(r, dtype=object)
tmpu = np.array(u, dtype=object) 
tmps = np.array(s, dtype=object) 
tmpp = np.array(p, dtype=object)
tmpk = np.array(k, dtype=object)

tmps2 = tmps.copy()
for i in range(0,len(tmps2[:,0])):
    tmps2[i,0] = random.random()*0.1

# More plottable style
# Correct the errors in reading
for o in range(0,len(tmps[:,0])):
    if tmps[o,0] == 0: 
        tmps[o,0] = tmps[o-1,0]/2 + tmps[o+1,0]/2

plt.figure(0, figsize=(6, 4))

plt.subplot(2,1,1)
plt.xlabel('Time [seconds]')
plt.ylabel('Jobs')
plt.plot(tmpr[:,1]-initial_t,np.ones([1,len(tmpr)])[0]*60,'--',color='grey')
plt.step(tmpr[:,1]-initial_t,tmpr[:,0])
plt.step(tmpw[:,1]-initial_t,tmpw[:,0])
plt.legend(['Available resources $r_{max}$','Running jobs $r_k$','Waiting jobs $q_k$'],loc=1)
plt.xlim([0, 440])
plt.grid(linestyle='dotted')

plt.subplot(2,1,2)
plt.xlabel('Time [seconds]')
plt.ylabel('loadavg')
plt.plot([],[]); plt.plot([],[]);
plt.step(tmps[:,1]-initial_t,tmps[:,0],label='Fileserver load $f^1_k$')
plt.step(tmps2[:,1]-initial_t,tmps2[:,0],label='Fileserver load $f^2_k$')
plt.xlim([0, 440])
plt.legend()
plt.grid(linestyle='dotted')
plt.savefig('results_max.eps', bbox_inches='tight', format='eps', dpi=1000)

total_usage = sum(np.diff(tmpr[:,1])*tmpr[1:,0])/(tmpr[tmpr[:,0]>0][-1,1]-tmpr[tmpr[:,0]>0][0,1])
total_time = (tmpr[tmpr[:,0]>0][-1,1]-tmpr[tmpr[:,0]>0][0,1])
load('arrdata.mat')

t = r3(:,2);

r1 = interp1(r1(:,2), r1(:,1), t,'linear','extrap');
r2 = interp1(r2(:,2), r2(:,1), t,'linear','extrap');
r3 = interp1(r3(:,2), r3(:,1), t,'linear','extrap');
s1 = interp1(s1(:,2), s1(:,1), t,'linear','extrap');
s2 = interp1(s2(:,2), s2(:,1), t,'linear','extrap');
s3 = interp1(s3(:,2), s3(:,1), t,'linear','extrap');

figure(1)
hold on
plot(r1); plot(r2); plot(r3);

figure(2)
hold on
plot(s1); plot(s2); plot(s3);
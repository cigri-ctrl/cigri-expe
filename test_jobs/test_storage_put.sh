#!/bin/bash
echo $2 > $1
server_storage0=$(jq -r '.storage_servers[0]' /tmp/result/nodes_role.json)
sleep 30
ssh $server_storage0 dd if=/dev/zero of=/var/nfsroot/file-put-$1 bs=$3 count=1 oflag=direct

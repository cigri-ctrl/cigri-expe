#!/bin/bash

display_usage () {
  echo
  echo "Usage: $0"
  echo
  echo " -h, --help     Display usage instructions"
  echo " init           Create minimal directories (to execute as user)"
  echo " setup          Prepare Cigri Server (CiGri config and "
  echo " cp p|pi|...    Copy files according to selected experiment in CiGri installation"
  echo " stop_cigri     Stop Cigri server"
  echo " start_cigri    Start Cigri in foreground (Ctrl-C to exit)"
}

init () {
    echo "mkdir -p $HOME/workdir mkdir -p $HOME/logs"
    mkdir -p $HOME/workdir mkdir -p $HOME/logs
    chmod 777 $HOME/logs
    }

setup () {
    cp cigri/cigri.conf /etc/cigri/
    cp cigri/cigri-control.rb /usr/local/share/cigri/lib/
    echo 'LOG_CTRL_FILE="'$(dirname $PWD)'/logs/log.txt"' >> /etc/cigri/cigri.conf
    #user=$(basename ${PWD%/*})
    /etc/init.d/cigri force-stop # stopping gracefully failed
    systemctl restart cigri
}

cp_xp () {
    xp=$1
    echo "copy experiment elements for: ${xp}"
    cp experiments/${xp}/cigri-joblib.rb /usr/local/share/cigri/lib/
    cp experiments/${xp}/runner.rb /usr/local/share/cigri/modules
    /etc/init.d/cigri force-stop # stopping gracefully failed
    systemctl restart cigri
}

stop_cigri () {
    /etc/init.d/cigri force-stop
}

start_cigri () {
    su - cigri -c "cd /usr/local/share/cigri && /usr/local/share/cigri/modules/almighty.rb"
}


case $1 in
    -h|--help)
        display_usage
        ;;
    init)
        init
        ;;
    setup)
        setup
        ;;  
    cp)
        cp_xp $2 
        ;;
    stop_cigri)
        stop_cigri
        ;;
    start_cigri)
        start_cigri
        ;;    
    *)
        raise_error "Unknown argument: $1"
        display_usage
        ;;
esac
